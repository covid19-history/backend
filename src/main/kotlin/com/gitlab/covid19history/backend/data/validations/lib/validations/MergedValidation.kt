package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import com.gitlab.covid19history.backend.data.validations.lib.Validation

class MergedValidation<TRecord, P>(
    private val a: Validation<TRecord, P>,
    private val b: Validation<TRecord, P>
) : Validation<TRecord, P> {
    override fun validate(value: P, ctx: VContext<TRecord, P>): VResult<P> {
        val resultA = a.validate(value, ctx)
        val resultB = b.validate(value, ctx)
        return VResult.merge(resultA, resultB)
    }
}