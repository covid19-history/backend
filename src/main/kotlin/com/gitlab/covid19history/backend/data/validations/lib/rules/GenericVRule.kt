package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.validations.lib.RuleFn
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule

class GenericVRule<TRecord, P>(
    override val message: String,
    private  val test: RuleFn<TRecord, P>
) : VRule<TRecord, P> {
    override fun test(value: P, ctx: VContext<TRecord, P>): Boolean {
        return ctx.test(value)
    }
}