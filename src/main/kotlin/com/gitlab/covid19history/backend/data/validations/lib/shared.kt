package com.gitlab.covid19history.backend.data.validations.lib

import kotlin.reflect.KProperty1

typealias CtxFn<TRecord, P, R> = VContext<TRecord, P>.(value: P) -> R
typealias RuleFn<TRecord, P>   = CtxFn<TRecord, P, Boolean>

typealias VFieldRules<TRecord, P> = Map<KProperty1<P, *>, Validation<TRecord, *>>
fun <TRecord, P> mutableFieldRulesOf() =
    mutableMapOf<KProperty1<P, *>, Validation<TRecord, *>>()

typealias VFailedFields<T> = Map<KProperty1<T, *>, VResult.Failure<*>>
fun <P> mutableFailedFieldsOf() =
    mutableMapOf<KProperty1<P, *>, VResult.Failure<*>>()