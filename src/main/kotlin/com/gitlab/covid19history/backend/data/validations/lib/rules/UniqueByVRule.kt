package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule
import com.gitlab.covid19history.backend.data.validations.lib.context.NestedVContext
import kotlinx.coroutines.runBlocking
import kotlin.reflect.KProperty1

class UniqueByVRule<TRecord, P : Iterable<E>, E>(
    private val prop: KProperty1<E, *>
) : VRule<TRecord, P> {
    override val message = "exists multiple times"
    override fun test(value: P, ctx: VContext<TRecord, P>): Boolean {
        return value.distinctBy(prop).size == value.count()
    }
}