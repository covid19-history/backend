package com.gitlab.covid19history.backend.data.validations.lib

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.builders.NestedVBuilder
import com.gitlab.covid19history.backend.data.validations.lib.rules.*
import com.gitlab.covid19history.backend.data.validations.lib.validations.*
import org.litote.kmongo.Id
import java.time.temporal.TemporalAccessor
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

interface VBuilder<TRecord : Any, P> {
    val type: KClass<TRecord>

    fun build(): Validation<TRecord, P>

    fun test(rule: VRule<TRecord, P>): VRule<TRecord, P>
    fun include(validation: Validation<TRecord, P>)
    fun include(validation: TopLevelValidation<in P>)
    infix fun <NextP> KProperty1<P, NextP>.with(validation: Validation<TRecord, NextP>)
    infix fun <NextP> KProperty1<P, NextP>.with(validation: TopLevelValidation<in NextP>)

    operator fun <NextP> KProperty1<P, NextP>.invoke(init: VBuilder<TRecord, NextP>.() -> Unit) {
        val builder = NestedVBuilder<TRecord, NextP>(type)
        builder.init()
        with(builder.build())
    }

    infix fun <NextP> KProperty1<P, NextP?>.ifPresent(validation: TopLevelValidation<NextP>) {
        with(IfPresentValidation(validation.asNested()))
    }

    infix fun <NextP> KProperty1<P, NextP?>.ifPresent(init: VBuilder<TRecord, NextP>.() -> Unit) {
        val builder = NestedVBuilder<TRecord, NextP>(type)
        builder.init()
        with(IfPresentValidation(builder.build()))
    }

    fun test(message: String, test: RuleFn<TRecord, P>) =
        test(GenericVRule(message, test))

    fun VBuilder<TRecord, String>.notBlank(allowNull: Boolean = false) =
        @Suppress("UNCHECKED_CAST")
        test(NotBlankVRule as VRule<TRecord, P>)

    fun equal(provide: CtxFn<TRecord, P, Comparable<P>>) =
        test(EqualVRule(provide))

    fun notEqual(provide: CtxFn<TRecord, P, Comparable<P>>) =
        test(NotEqualVRule(provide))

    fun <P : TemporalAccessor> VBuilder<TRecord, P>.before(provide: CtxFn<TRecord, P, Comparable<P>>) =
        test(BeforeVRule(provide))

    fun <P : TemporalAccessor> VBuilder<TRecord, P>.after(provide: CtxFn<TRecord, P, Comparable<P>>) =
        test(AfterVRule(provide))

    infix fun <E> KProperty1<P, Iterable<E>>.forEach(init: VBuilder<TRecord, E>.() -> Unit) {
        val builder = NestedVBuilder<TRecord, E>(type)
        builder.init()
        with(ForEachValidation(IfPresentValidation(builder.build())))
    }

    infix fun <E, P : Iterable<E>> VBuilder<TRecord, P>.forEach(init: VBuilder<TRecord, E>.() -> Unit) {
        val builder = NestedVBuilder<TRecord, E>(type)
        builder.init()
        include(ForEachValidation<TRecord, P, E>(IfPresentValidation(builder.build())))
    }

    fun <E, P : Iterable<E>, C> VBuilder<TRecord, P>.uniqueBy(uniqueProp: KProperty1<E, C>, top: CtxFn<TRecord, C, P>) {
        // test(UniqueByVRule(prop))

        forEach {
            uniqueProp {
                test("is not unique") {
                    top(it).distinctBy(uniqueProp).size == top(it).count()
                }
            }
        }
    }

    fun <P : Id<M>, M : Model<M>> VBuilder<TRecord, P>.exists(repo: Repo<M>) {
        test(IdExistsVRule(repo))
    }

    fun If(condition: CtxFn<TRecord, P, Boolean>, init: VBuilder<TRecord, P>.() -> Unit) {
        val builder = NestedVBuilder<TRecord, P>(type)
        builder.init()
        include(ConditionalValidation(condition, builder.build()))
    }
}