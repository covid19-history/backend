package com.gitlab.covid19history.backend.data.repos.utils

import com.mongodb.BasicDBObject
import org.bson.conversions.Bson
import org.litote.kmongo.path
import kotlin.reflect.KProperty1

fun bson(vararg pairs: Pair<Any, Any>): Bson {
    val entries = mapOf(*pairs).mapKeys { (key, _) ->
        if (key is KProperty1<*, *>) {
            key.path()
        } else {
            key.toString()
        }
    }
    return BasicDBObject(entries)
}

fun matchIgnoreCase(value: String) =
    Regex("^${Regex.escape(value)}\$", RegexOption.IGNORE_CASE)

val emptyBson: Bson = bson()