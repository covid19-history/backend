package com.gitlab.covid19history.backend.data.repos.utils

import com.gitlab.covid19history.backend.data.models.Model

interface RepoCallbacks<T : Model<T>> {
    suspend fun beforeDelete(record: T) {}
    suspend fun afterDelete(record: T) {}
}