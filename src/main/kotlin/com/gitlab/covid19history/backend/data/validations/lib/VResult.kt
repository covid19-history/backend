package com.gitlab.covid19history.backend.data.validations.lib

import kotlin.reflect.KProperty1


sealed class VResult<T> {
    abstract val isSuccess: Boolean
    val isFailure get() = !isSuccess

    inline fun <R> ifFailure(handle: (result: Failure<T>) -> R): R? {
        return if (this is Failure) {
            handle(this)
        } else {
            null
        }
    }

    object Success : VResult<Any>() {
        override val isSuccess = true
        operator fun <T> invoke(): VResult<T> {
            @Suppress("UNCHECKED_CAST")
            return Success as VResult<T>
        }
    }

    class Failure<T>(
        val failedRules:  Set<VRule<*, T>>,
        val failedFields: VFailedFields<T>
    ) : VResult<T>() {
        override val isSuccess = false

    }

    companion object {
        operator fun <T> invoke(rules: Set<VRule<*, T>> = emptySet(), fields: VFailedFields<T> = emptyMap()): VResult<T> {
            return if (rules.isEmpty() && fields.isEmpty()) {
                Success()
            } else {
                Failure(rules, fields)
            }
        }

        fun <T> merge(resultA: VResult<T>, resultB: VResult<T>): VResult<T> {
            if (resultA.isSuccess) {
                return resultB
            }
            if (resultB.isSuccess) {
                return resultA
            }
            val failA = resultA as Failure<T>
            val failB = resultB as Failure<T>
            return Failure(
                failA.failedRules + failB.failedRules,
                this.mergeFields(failA.failedFields, failB.failedFields)
            )
        }

        private fun <T> mergeFields(fieldsA: VFailedFields<T>, fieldsB: VFailedFields<T>): VFailedFields<T> {
            val fields = mutableMapOf<KProperty1<T, *>, Failure<*>>()
            fieldsA.forEach { (prop, failureA) ->
                val failureB = fieldsB[prop]
                if (failureB != null) {
                    @Suppress("UNCHECKED_CAST")
                    val resultA = failureA as VResult<Any?>
                    @Suppress("UNCHECKED_CAST")
                    val resultB = failureB as VResult<Any?>
                    @Suppress("UNCHECKED_CAST")
                    fields[prop] = merge(resultA, resultB) as Failure<*>
                }
            }
            return fields
        }
    }
}