package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.Id

class IdExistsVRule<TRecord, P : Id<M>, M : Model<M>>(
    private val repo: Repo<M>
) : VRule<TRecord, P> {
    override val message = "does not exist"
    override fun test(value: P, ctx: VContext<TRecord, P>): Boolean {
        return runBlocking {
            repo.exists(value)
        }
    }
}