package com.gitlab.covid19history.backend.data.validations.lib

import com.gitlab.covid19history.backend.data.validations.lib.rules.VRuleDelegate

interface VRule<TRecord, P> {
    val message: String
    fun test(value: P, ctx: VContext<TRecord, P>): Boolean

    fun report(message: String): VRule<TRecord, P> {
        return VRuleDelegate(message, this)
    }
}