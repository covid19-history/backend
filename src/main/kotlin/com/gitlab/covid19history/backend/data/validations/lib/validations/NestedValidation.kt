package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.data.validations.lib.*
import com.gitlab.covid19history.backend.data.validations.lib.context.NestedVContext
import kotlin.reflect.KProperty1

class NestedValidation<TRecord, P>(
    private val rules:  Set<VRule<TRecord, P>>,
    private val fields: VFieldRules<TRecord, P>
) : Validation<TRecord, P> {
    override fun validate(value: P, ctx: VContext<TRecord, P>): VResult<P> {
        val failedRules  = rules.filter { !it.test(value, ctx) }.toSet()
        val failedFields = fields
            .mapValues { validateField(it, value, ctx) }
            .filter    { (_, it)     -> it.isFailure }
            .mapValues { (_, result) -> result as VResult.Failure }
        return VResult(failedRules, failedFields)
    }

    private fun validateField(entry: Map.Entry<KProperty1<P, *>, Validation<TRecord, *>>, value: P, ctx: VContext<TRecord, P>) =
        @Suppress("UNCHECKED_CAST")
        validateField(entry.key, entry.value as Validation<TRecord, Any?>, value, ctx)

    private fun <NextP> validateField(prop: KProperty1<P, NextP>, validation: Validation<TRecord, NextP>, value: P, ctx: VContext<TRecord, P>): VResult<NextP> {
        val fieldValue = prop.get(value)
        @Suppress("UNCHECKED_CAST")
        val fieldCtx = NestedVContext.from(ctx, prop) as NestedVContext<TRecord, P, Any?>
        return if (fieldCtx.hasChanged) {
            @Suppress("UNCHECKED_CAST")
            val fieldValidation = validation as Validation<TRecord, Any?>
            @Suppress("UNCHECKED_CAST")
            fieldValidation.validate(fieldValue, fieldCtx) as VResult<NextP>
        } else {
            // skip field, it's unchanged
            VResult.Success()
        }
    }
}