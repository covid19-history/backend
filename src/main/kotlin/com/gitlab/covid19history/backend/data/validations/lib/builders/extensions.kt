package com.gitlab.covid19history.backend.data.validations.lib.builders

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.VBuilder
import com.gitlab.covid19history.backend.data.validations.lib.VRule
import com.gitlab.covid19history.backend.data.validations.lib.rules.UniqueIgnoreCaseVRule
import com.gitlab.covid19history.backend.data.validations.lib.rules.UniqueVRule

fun <TRecord : Model<TRecord>, P> VBuilder<TRecord, P>.unique(): VRule<TRecord, P> {
    return test(UniqueVRule(Repo.find(type)))
}

fun <TRecord : Model<TRecord>, String> VBuilder<TRecord, String>.uniqueIgnoreCase(): VRule<TRecord, String> {
    @Suppress("UNCHECKED_CAST")
    return test(UniqueIgnoreCaseVRule(Repo.find(type)) as VRule<TRecord, String>)
}