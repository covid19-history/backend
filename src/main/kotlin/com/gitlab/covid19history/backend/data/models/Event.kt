package com.gitlab.covid19history.backend.data.models

import com.gitlab.covid19history.backend.utils.DateTime
import com.gitlab.covid19history.backend.utils.nowDateTime
import org.bson.codecs.pojo.annotations.BsonId
import org.litote.kmongo.Id
import org.litote.kmongo.newId

data class Event(
    @BsonId
    override val id: Id<Event> = newId(),

    val title:       String,
    val summary:     String? = null,
    val description: String? = null,
    val time:        DateTime,
    val sources:     List<Source>,
    val category:    Category,

    val reactions: Reactions = Reactions.empty,

    override val updatedAt: DateTime = nowDateTime(),
    override val createdAt: DateTime = updatedAt
) : Model<Event> {

    data class Source(
        val name: String,
        val link: String
    )

    enum class Category {
        NEWS,
        POSITIVE,
        NEGATIVE,
    }

    data class Reactions(
        val claps:     Int,
        val hearts:    Int,
        val thumbUps:  Int,
        val thumbDowns: Int,
        val smiles:    Int,
        val frowns:    Int
    ) {
        companion object {
            val empty = Reactions(
                claps      = 0,
                hearts     = 0,
                thumbUps   = 0,
                thumbDowns = 0,
                smiles     = 0,
                frowns     = 0
            )
        }
    }

}