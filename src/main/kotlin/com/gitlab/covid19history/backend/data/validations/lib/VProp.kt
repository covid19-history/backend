package com.gitlab.covid19history.backend.data.validations.lib

class VProp<TRecord, V>(
    private val compute: CtxFn<TRecord, TRecord, V>
) {
    fun get(ctx: VContext<TRecord, TRecord>): V {
        return ctx.compute(ctx.record)
    }

    override fun hashCode(): Int {
        return compute.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return this === other
    }

    override fun toString(): String {
        return "${VProp::class.simpleName}($compute)"
    }
}