package com.gitlab.covid19history.backend.data.repos.utils

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
open class DBOptions {
    @Value("\${mongo.host}")
    lateinit var host: String

    @Value("\${mongo.loginDB}")
    lateinit var loginDB: String

    @Value("\${mongo.username}")
    lateinit var username: String

    @Value("\${mongo.password}")
    lateinit var password: String
}