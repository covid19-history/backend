package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.validations.lib.CtxFn
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule

class EqualVRule<TRecord, P>(
    private val provide: CtxFn<TRecord, P, Comparable<P>>
) : VRule<TRecord, P> {
    override val message: String = "is not the correct value"
    override fun test(value: P, ctx: VContext<TRecord, P>): Boolean {
        return ctx.provide(value) == value
    }
}