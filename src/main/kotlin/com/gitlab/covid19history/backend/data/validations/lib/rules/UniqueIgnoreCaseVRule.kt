package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule
import com.gitlab.covid19history.backend.data.validations.lib.context.NestedVContext
import kotlinx.coroutines.runBlocking

class UniqueIgnoreCaseVRule<TRecord : Model<TRecord>>(
    private val repo: Repo<TRecord>
) : VRule<TRecord, String> {
    override val message = "must be unique"
    override fun test(value: String, ctx: VContext<TRecord, String>): Boolean {
        return runBlocking {
            if (ctx is NestedVContext<*, *, *>) {
                repo.isUniqueIgnoreCase(ctx.record, value, *ctx.props)
            } else {
                repo.isUniqueIgnoreCase(ctx.record, value)
            }
        }
    }
}