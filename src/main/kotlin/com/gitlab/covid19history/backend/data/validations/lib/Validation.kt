package com.gitlab.covid19history.backend.data.validations.lib

import com.gitlab.covid19history.backend.data.validations.lib.builders.TopLevelVBuilder
import com.gitlab.covid19history.backend.data.validations.lib.validations.TopLevelValidation

interface Validation<TRecord, P> {
    fun validate(value: P, ctx: VContext<TRecord, P>): VResult<P>

    companion object {
        inline operator fun <reified TRecord : Any> invoke(init: TopLevelVBuilder<TRecord>.() -> Unit): TopLevelValidation<TRecord> {
            val builder = TopLevelVBuilder<TRecord>()
            builder.init()
            return builder.build()
        }
    }
}