package com.gitlab.covid19history.backend.data.validations

import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.data.validations.lib.Validation
import com.gitlab.covid19history.backend.utils.nowDateTime

val validateEvent = Validation<Event> {
    Event::title {
        notBlank()
    }
    Event::summary ifPresent {
        notBlank()
    }
    Event::description ifPresent {
        notBlank()
    }
    Event::time {
        before { nowDateTime() }
    }
    Event::sources forEach  {
        Event.Source::name {
            notBlank()
        }
        Event.Source::link {
            notBlank()
        }
    }
}