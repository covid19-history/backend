package com.gitlab.covid19history.backend.data.validations.lib

@Target(
    AnnotationTarget.FIELD,
    AnnotationTarget.PROPERTY
)
@Retention(AnnotationRetention.RUNTIME)
annotation class VName(val value: String)