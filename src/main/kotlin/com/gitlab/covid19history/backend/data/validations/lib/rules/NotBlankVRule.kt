package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule

object NotBlankVRule : VRule<Any, String> {
    override val message = "must not be blank"
    override fun test(value: String, ctx: VContext<Any, String>): Boolean {
        return value.isNotBlank()
    }
}