package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import com.gitlab.covid19history.backend.data.validations.lib.Validation

class IfPresentValidation<TRecord, P>(
    private val delegate: Validation<TRecord, P>
) : Validation<TRecord, P?> {
    override fun validate(value: P?, ctx: VContext<TRecord, P?>): VResult<P?> {
        return if (value == null) {
            VResult.Success()
        } else {
            @Suppress("UNCHECKED_CAST")
            delegate.validate(value, ctx as VContext<TRecord, P>) as VResult<P?>
        }
    }
}