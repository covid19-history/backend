package com.gitlab.covid19history.backend.data.validations.lib.builders

import com.gitlab.covid19history.backend.data.validations.lib.CtxFn
import com.gitlab.covid19history.backend.data.validations.lib.VBuilder
import com.gitlab.covid19history.backend.data.validations.lib.VProp
import com.gitlab.covid19history.backend.data.validations.lib.Validation
import com.gitlab.covid19history.backend.data.validations.lib.validations.TopLevelValidation
import kotlin.reflect.KProperty1

class TopLevelVBuilder<TRecord : Any>(
    private val delegate: NestedVBuilder<TRecord, TRecord>
) : VBuilder<TRecord, TRecord> by delegate {
    override fun build(): TopLevelValidation<TRecord> {
        return TopLevelValidation(this.delegate.build())
    }

    fun <V> prop(compute: CtxFn<TRecord, TRecord, V>): VProp<TRecord, V> {
        return VProp(compute)
    }

    override fun <NextP> KProperty1<TRecord, NextP>.with(validation: Validation<TRecord, NextP>) {
        delegate.run {
            this@with with validation
        }
    }

    override fun <NextP> KProperty1<TRecord, NextP>.with(validation: TopLevelValidation<in NextP>) {
        delegate.run {
            this@with with validation
        }
    }

    companion object {
        inline operator fun <reified TRecord : Any> invoke(): TopLevelVBuilder<TRecord> {
            return TopLevelVBuilder(NestedVBuilder(TRecord::class))
        }
    }
}