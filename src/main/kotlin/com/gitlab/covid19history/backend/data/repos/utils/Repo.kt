package com.gitlab.covid19history.backend.data.repos.utils

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.utils.props.IndexProperty
import com.mongodb.client.model.Filters.*
import com.mongodb.client.model.InsertOneModel
import com.mongodb.client.model.ReplaceOneModel
import org.bson.conversions.Bson
import org.litote.kmongo.Id
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.util.KMongoUtil
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

abstract class Repo<T : Model<T>> : BaseRepo<T>, RepoCallbacks<T> {
    protected abstract val col: CoroutineCollection<T>

    protected open val sort: Bson = emptyBson

    suspend fun count(): Long {
        return col.countDocuments()
    }

    suspend fun list(): List<T> {
        return col.find().sort(sort).toList()
    }

    suspend fun list(filter: Bson): List<T> {
        return col.find(filter).toList()
    }

    suspend fun list(ids: Iterable<Id<T>>): List<T> {
        return col.find(`in`("_id", ids)).toList()
    }

    suspend fun find(id: Id<T>): T? {
        return col.findOne(id.filter)
    }

    suspend fun mustFind(id: Id<T>): T {
        return find(id)
            ?: error("no such record: $id")
    }

    suspend fun exists(id: Id<T>): Boolean {
        return col.countDocuments(id.filter) == 1L
    }

    suspend fun create(record: T) {
        col.insertOne(record)
    }

    suspend fun createAll(records: Iterable<T>) {
        val requests = records.map { InsertOneModel(it) }
        if (requests.isEmpty()) {
            return
        }
        col.bulkWrite(requests)
    }

    suspend fun update(record: T) {
        col.replaceOne(record.id.filter, record)
    }

    suspend fun updateAll(vararg records: T) {
        updateAll(records.asIterable())
    }

    suspend fun updateAll(records: Iterable<T>) {
        val requests = records.map { ReplaceOneModel(it.id.filter, it) }
        if (requests.isEmpty()) {
            return
        }
        col.bulkWrite(requests)
    }

    suspend fun delete(id: Id<T>): Boolean {
        val record = find(id)
            ?: return false
        beforeDelete(record)
        val ok = col.deleteOneById(id).deletedCount == 1L
        if (ok) {
            afterDelete(record)
        }
        return ok
    }

    suspend fun isUnique(record: T, value: Any?, vararg path: KProperty1<*, *>): Boolean {
        val key = path
            .filter { it !is IndexProperty }
            .joinToString(".") { it.name }
        return col.countDocuments(and(
            eq(key, value),
            ne("_id", record.id)
        )) == 0L
    }

    suspend fun isUniqueIgnoreCase(record: T, value: String, vararg path: KProperty1<*, *>): Boolean {
        val key = path
            .filter { it !is IndexProperty }
            .joinToString(".") { it.name }
        return col.countDocuments(and(
            regex(key, Regex("^${Regex.escape(value)}\$", RegexOption.IGNORE_CASE).toPattern()),
            ne("_id", record.id)
        )) == 0L
    }

    private val Id<T>.filter get() =
        eq("_id", this)

    companion object {
        fun <T : Model<T>> find(type: KClass<T>): Repo<T> {
            return object : Repo<T>() {
                override val col = DB.db.database.getCollection(
                    KMongoUtil.defaultCollectionName(type),
                    type.java
                ).coroutine
            }
        }
    }
}