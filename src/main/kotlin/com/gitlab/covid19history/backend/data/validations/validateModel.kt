package com.gitlab.covid19history.backend.data.validations

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.validations.lib.Validation


private typealias M = Model<*>

val validateModel = Validation<M> {
    M::updatedAt {
        // before { nowDateTime() }.report("must not be in the future")

        If({ isCreate }) {
            equal { record.createdAt }.report("must equal createdAt")
        }
        If({ isUpdate }) {
            after { original!!.updatedAt }.report("must be updated")
        }
    }
    M::createdAt {
        // before { nowDateTime() }.report("must not be in the future")

        If({ isCreate }) {
            equal { record.updatedAt }.report("must equal updatedAt")
        }
        If({ isUpdate }) {
            equal { original!!.updatedAt }.report("must not be changed")
        }
    }
}