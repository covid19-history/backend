package com.gitlab.covid19history.backend.data.validations.lib.builders

import com.gitlab.covid19history.backend.data.validations.lib.*
import com.gitlab.covid19history.backend.data.validations.lib.rules.VRuleDelegate
import com.gitlab.covid19history.backend.data.validations.lib.validations.MergedValidation
import com.gitlab.covid19history.backend.data.validations.lib.validations.NestedValidation
import com.gitlab.covid19history.backend.data.validations.lib.validations.TopLevelValidation
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

class NestedVBuilder<TRecord : Any, P>(override val type: KClass<TRecord>) : VBuilder<TRecord, P> {
    private val rules    = mutableSetOf<VRule<TRecord, P>>()
    private val fields   = mutableFieldRulesOf<TRecord, P>()
    private val includes = mutableSetOf<Validation<TRecord, P>>()

    override fun build(): Validation<TRecord, P> {
        val validation: Validation<TRecord, P> = NestedValidation(rules, fields)
        return if (includes.isEmpty()) {
            validation
        } else {
            includes.fold(validation) { a, b -> MergedValidation(a, b) }
        }
    }

    override fun include(validation: Validation<TRecord, P>) {
        this.includes += validation
    }

    override fun include(validation: TopLevelValidation<in P>) {
        @Suppress("UNCHECKED_CAST")
        this.includes += validation.asNested<TRecord>() as Validation<TRecord, P>
    }

    override fun <NextP> KProperty1<P, NextP>.with(validation: Validation<TRecord, NextP>) {
        assign(validation)
    }

    override fun <NextP> KProperty1<P, NextP>.with(validation: TopLevelValidation<in NextP>) {
        assign(validation.asNested())
    }

    override fun test(rule: VRule<TRecord, P>): VRule<TRecord, P> {
        val delegateRule = VRuleDelegate(rule.message, rule)
        rules += delegateRule
        return delegateRule
    }


    @Suppress("UNCHECKED_CAST")
    private fun <NextP> KProperty1<P, NextP>.assign(validation: Validation<TRecord, NextP>) {
        val existingValidation = fields[this] as Validation<TRecord, NextP>?
        fields[this] = if (existingValidation == null) {
            validation
        } else {
            MergedValidation(existingValidation, validation)
        }
    }
}