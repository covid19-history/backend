package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.models.Model
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VRule
import com.gitlab.covid19history.backend.data.validations.lib.context.NestedVContext
import kotlinx.coroutines.runBlocking

class UniqueVRule<TRecord : Model<TRecord>, P>(
    private val repo: Repo<TRecord>
) : VRule<TRecord, P> {
    override val message = "must be unique"
    override fun test(value: P, ctx: VContext<TRecord, P>): Boolean {
        return runBlocking {
            testNested(value, ctx)
        }
    }

    private suspend fun testNested(value: P, ctx: VContext<TRecord, P>): Boolean {
        return if (ctx is NestedVContext<*, *, *>) {
            repo.isUnique(ctx.record, value, *ctx.props)
        } else {
            repo.isUnique(ctx.record, value)
        }
    }
}