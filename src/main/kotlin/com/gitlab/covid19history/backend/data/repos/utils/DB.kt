package com.gitlab.covid19history.backend.data.repos.utils

import com.gitlab.covid19history.backend.Spring
import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo
import org.springframework.stereotype.Component

@Component
object DB {
    private val client = run {
        val opts = Spring.bean<DBOptions>()
        val credentials = MongoCredential.createCredential(
            opts.username,
            opts.loginDB,
            opts.password.toCharArray()
        )
        val options = MongoClientSettings.builder()
            .applyConnectionString(ConnectionString("mongodb://${opts.host}/admin"))
            .credential(credentials)
            .build()

        KMongo.createClient(options).coroutine
    }

    val db = client.getDatabase("covid19-history")

    inline fun <reified T : Any> collection(name: String? = null): CoroutineCollection<T> {
        return if (name == null) {
            db.getCollection()
        } else {
            db.getCollection(name)
        }
    }
}