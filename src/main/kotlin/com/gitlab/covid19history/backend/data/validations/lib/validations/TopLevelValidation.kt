package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import com.gitlab.covid19history.backend.data.validations.lib.Validation
import com.gitlab.covid19history.backend.data.validations.lib.context.TopLevelVContext

@Suppress("UNCHECKED_CAST")
class TopLevelValidation<TRecord>(
    val delegate: Validation<TRecord, TRecord>
) : Validation<TRecord, TRecord> {
    override fun validate(value: TRecord, ctx: VContext<TRecord, TRecord>) =
        // Discard the old context, since we don't want to use a nested context.
        // We will create our own top level context.
        execute(value, TopLevelVContext(value, ctx.run { ctx.original?.propertyValue }))

    operator fun invoke(value: TRecord) =
        execute(value, TopLevelVContext(value, null))

    operator fun invoke(value: TRecord, original: TRecord): VResult<TRecord> {
        return execute(value, TopLevelVContext(value, original))
    }


    private fun execute(value: TRecord, ctx: TopLevelVContext<TRecord>) =
        delegate.validate(value, ctx)

    fun <TNestedRecord> asNested(): Validation<TNestedRecord, TRecord> {
        // while this is not 100% type safe,
        // it's ok to do since we always create our own contexts,
        // and ensure that TNestedRecord will be equal to TRecord in our nested records.
        return this as Validation<TNestedRecord, TRecord>
    }
}