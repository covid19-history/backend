package com.gitlab.covid19history.backend.data.repos

import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.data.repos.utils.DB
import com.gitlab.covid19history.backend.data.repos.utils.Repo
import com.gitlab.covid19history.backend.initEvents
import com.gitlab.covid19history.backend.utils.DateTime
import com.gitlab.covid19history.backend.utils.ifPresent
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.gt
import org.litote.kmongo.lt

object EventRepo : Repo<Event>() {
    override val col = DB.collection<Event>()

    init {
        val count = runBlocking { count() }
        if (count == 0L) {
            runBlocking { initEvents() }
        }
    }

    suspend fun list(after: DateTime? = null, before: DateTime? = null): List<Event> {
        val filterAfter = after.ifPresent {
            Event::time gt it
        }
        val filterBefore = before.ifPresent {
            Event::time lt it
        }
        return col.find(org.litote.kmongo.and(filterAfter, filterBefore)).toList()
    }
}