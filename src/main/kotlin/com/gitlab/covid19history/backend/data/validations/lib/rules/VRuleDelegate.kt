package com.gitlab.covid19history.backend.data.validations.lib.rules

import com.gitlab.covid19history.backend.data.validations.lib.VRule

data class VRuleDelegate<TRecord, P>(
    override var message: String,
    private val delegate: VRule<TRecord, P>
) : VRule<TRecord, P> by delegate {
    override fun report(message: String): VRule<TRecord, P> {
        this.message = message
        return this
    }
}