package com.gitlab.covid19history.backend.data.repos.utils

import com.gitlab.covid19history.backend.data.models.BaseModel

interface BaseRepo<T : BaseModel<T>>