package com.gitlab.covid19history.backend.data.validations.lib

interface VContext<TRecord, P> {
    val record:   TRecord
    val original: TRecord?

    val isUpdate: Boolean get() = original != null
    val isCreate: Boolean get() = original == null

    val hasChanged: Boolean

    operator fun <V> VProp<TRecord, V>.invoke(): V
    val TRecord.propertyValue: P?
}