package com.gitlab.covid19history.backend.data.validations.lib.context

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VProp

class TopLevelVContext<TRecord>(
    override val record:   TRecord,
    override val original: TRecord?
) : VContext<TRecord, TRecord> {

    override fun <V> VProp<TRecord, V>.invoke(): V {
        return this.get(this@TopLevelVContext)
    }

    override val hasChanged: Boolean get() {
        return this.record != this.original
    }

    override val TRecord.propertyValue get() = this
}