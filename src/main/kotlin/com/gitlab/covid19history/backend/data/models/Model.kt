package com.gitlab.covid19history.backend.data.models

import com.gitlab.covid19history.backend.utils.DateTime
import org.litote.kmongo.Id

interface Model<T : Model<T>> : IdModel<T>,
    ObservableModel<T>

interface BaseModel<T : BaseModel<T>>

interface IdModel<T : IdModel<T>> :
    BaseModel<T> {
    val id: Id<T>
}

interface ObservableModel<T : ObservableModel<T>> :
    BaseModel<T> {
    val updatedAt: DateTime
    val createdAt: DateTime
}