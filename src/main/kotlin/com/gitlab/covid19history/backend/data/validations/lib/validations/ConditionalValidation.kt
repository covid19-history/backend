package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.data.validations.lib.CtxFn
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import com.gitlab.covid19history.backend.data.validations.lib.Validation

class ConditionalValidation<TRecord, P>(
    val condition: CtxFn<TRecord, P, Boolean>,
    val delegate:  Validation<TRecord, P>
) : Validation<TRecord, P> {
    override fun validate(value: P, ctx: VContext<TRecord, P>): VResult<P> {
        return if (ctx.condition(value)) {
            delegate.validate(value, ctx)
        } else {
            VResult.Success()
        }
    }

}