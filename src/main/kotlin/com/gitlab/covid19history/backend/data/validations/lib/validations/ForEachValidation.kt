package com.gitlab.covid19history.backend.data.validations.lib.validations

import com.gitlab.covid19history.backend.utils.props.IndexProperty
import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import com.gitlab.covid19history.backend.data.validations.lib.Validation
import com.gitlab.covid19history.backend.data.validations.lib.context.NestedVContext
import com.gitlab.covid19history.backend.data.validations.lib.mutableFailedFieldsOf

class ForEachValidation<TRecord, P : Iterable<E>, E>(
    private val delegate: Validation<TRecord, E?>
) : Validation<TRecord, P> {
    override fun validate(value: P, ctx: VContext<TRecord, P>): VResult<P> {
        val errors = mutableFailedFieldsOf<P>()
        var index = -1;
        for (element in value) {
            index += 1
            if (element == null) {
                continue;
            }
            val prop = IndexProperty<P, E>(index) { i ->
                var currentIndex = 0;
                for (currentElement in this) {
                    if (currentIndex == i) {
                        return@IndexProperty currentElement
                    }
                    currentIndex += 1
                }
                return@IndexProperty null
            }
            val elementCtx = NestedVContext.from(ctx, prop)

            delegate.validate(element, elementCtx).ifFailure {
                errors[prop] = it
            }
        }
        return VResult(fields = errors)
    }
}