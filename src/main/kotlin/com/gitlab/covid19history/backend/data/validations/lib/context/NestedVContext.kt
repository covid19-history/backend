package com.gitlab.covid19history.backend.data.validations.lib.context

import com.gitlab.covid19history.backend.data.validations.lib.VContext
import com.gitlab.covid19history.backend.data.validations.lib.VProp
import kotlin.reflect.KProperty1

class NestedVContext<TRecord, PrevP, P> private constructor(
    override val record:      TRecord,
    override val original:    TRecord?,
    private  val previousCtx: VContext<TRecord, PrevP>,
    val prop: KProperty1<PrevP, P>
) : VContext<TRecord, P> {
    val props: Array<KProperty1<*, *>> get() {
        return if (previousCtx is NestedVContext<*, *, *>) {
            (previousCtx as NestedVContext<TRecord, *, PrevP>).props + prop
        } else {
            arrayOf(prop)
        }
    }

    override fun <V> VProp<TRecord, V>.invoke(): V {
        return previousCtx.run { this@invoke() }
    }

    override val hasChanged: Boolean get() {
        if (original == null) {
            return true
        }
        return record.propertyValue != original.propertyValue
    }

    override val TRecord.propertyValue: P? get() {
        val previousValue = previousCtx.run { propertyValue }
            ?: return null
        return prop.get(previousValue)
    }

    companion object {
        fun <TRecord, PrevP, P> from(ctx: VContext<TRecord, PrevP>, prop: KProperty1<PrevP, P>): NestedVContext<TRecord, PrevP, P> {
            return NestedVContext(
                record      = ctx.record,
                original    = ctx.original,
                previousCtx = ctx,
                prop        = prop
            )
        }
    }
}