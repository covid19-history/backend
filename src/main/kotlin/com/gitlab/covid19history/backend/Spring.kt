package com.gitlab.covid19history.backend

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
object Spring {
    lateinit var applicationContext: ApplicationContext

    @Autowired
    fun wireApplicationContext(applicationContext: ApplicationContext) {
        this.applicationContext = applicationContext
    }

    inline  fun <reified T> bean(): T {
        return applicationContext.getBean(T::class.java)
    }
}