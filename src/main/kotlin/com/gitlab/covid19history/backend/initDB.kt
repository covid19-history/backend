package com.gitlab.covid19history.backend

import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.data.models.Event.Category
import com.gitlab.covid19history.backend.data.repos.EventRepo
import com.gitlab.covid19history.backend.utils.DateTime

suspend fun initEvents() {
    val events = mutableListOf<Event>()
    events += Event(
        time     = DateTime.of(2019, 12, 1, 0, 0, 0),
        title    = "Patient 0",
        category = Category.NEGATIVE,
        sources  = listOf(
            Event.Source("The Lancet", "https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(20)30183-5/fulltext"),
            Event.Source("BBC", "https://www.bbc.com/future/article/20200221-coronavirus-the-harmful-hunt-for-covid-19s-patient-zero")
        )
    )
    events += Event(
        time     = DateTime.of(2019, 12, 31, 0, 0, 0),
        title    = "first signs",
        summary  = "China reports a cluster of pneumonia cases in Wuhan - cause unknown.",
        category = Category.NEGATIVE,
        sources  = listOf(
        )
    )
    events += Event(
        time        = DateTime.of(2020, 1, 3, 0, 0, 0),
        title       = "China starts investigating",
        summary     = "China starts further investigations into the virus outbreak.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("BBC", "https://www.bbc.com/news/world-asia-china-50984025")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 1, 9, 0, 0, 0),
        title       = "first confirmed death caused by coronavirus",
        summary     = "The first case of death attributed to COVID19 is reported in Wuhan.",
        description =
            "A 61-year-old man, which was conformed to have been infected with the virus, dies."
          + "He was admitted to a hospital in Wuhan, China, since the 27th December."
          + "At that time, he had a fever and a cough."
          + "He was later on, and till his death, attached to a ventilator."
        ,
        category    = Category.NEGATIVE,
        sources     = listOf(
            Event.Source("The New York Times", "https://www.nytimes.com/2020/01/23/world/asia/china-coronavirus.html")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 1, 23, 0, 0, 0),
        title       = "Wuhan gets quarantined",
        summary     = "The city of Wuhan is quarantined, stopping travel in and out of it.",
        description =
            "Wuhan, the center of the initial coronoavirus outbreak, is quarantined."
          + "This includes the prohibition of all travel in and out of the city.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("Channel News Asia", "https://www.channelnewsasia.com/news/asia/wuhan-virus-quarantine-city-flights-trains-china-12306684")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 1, 23, 0, 0, 0),
        title       = "the use of private vehicles in Wuhan is banned",
        summary     = "Chinese authorities ban the use of private vehicles in Wuhan",
        description =
             "The use of all private vehicles inside of Wuhan is banned by Chinese authorities."
           + "Only vehicles that are transporting critical supplies or emergency response vehicles are allowed to move within the city.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("ABC News AU", "https://www.abc.net.au/news/2020-01-26/australians-in-coronavirus-epicentre-wuhan-trying-to-get-out/11902006")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 2, 1, 0, 0, 0),
        title       = "first confirmed death caused by coronavirus outside of China",
        summary     = "The first death outside of China occurs in the Philippines.",
        description =
            "A 44-year-old man, infected with the coronavirus, dies in the Philippines."
          + "The man was a resident of Wuhan, China, which is the center of the initial virus outbreak.",
        category    = Category.NEGATIVE,
        sources     = listOf(
            Event.Source("The New York Times", "https://www.nytimes.com/2020/02/02/world/asia/philippines-coronavirus-china.html")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 2, 14, 0, 0, 0),
        title       = "Finding the Source",
        summary     = "The origins of the virus are investigated.",
        description =
            "The virus is tracked back to the Huanan Seafood Wholesale Market. It is thus expected to have a zoonotic origin."
          + "The strand of the virus that caused the outbreak is now known as SARS-CoV-2."
          + "It is closely related to bat coronavirus, pangolin coronavirus, and SARS-CoV.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("The New England Journal of Medicine", "https://www.nejm.org/doi/10.1056/NEJMe2001126"),
            Event.Source("European Centre for Disease Prevention and Control", "https://www.ecdc.europa.eu/sites/default/files/documents/SARS-CoV-2-risk-assessment-14-feb-2020.pdf"),
            Event.Source("M. C. Wong, S.J. Cregeen, N. J. Ajami, J. F. Petrosino", "https://www.biorxiv.org/content/10.1101/2020.02.07.939207v1")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 2, 24, 0, 0, 0),
        title       = "Patient 0 is found",
        description =
            "A study by chinese researcher dates the first coronavirus case back to December 1st."
          + "This is a whole month earlier than reported by the Chinese Government."
          + "Patient 0 does also not have a link to the Huanan Seafood Wholesale Market,"
          + "which seems to disprove the theory of the virus having originated there.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("The Lancet", "https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(20)30183-5/fulltext"),
            Event.Source("BBC", "https://www.bbc.com/future/article/20200221-coronavirus-the-harmful-hunt-for-covid-19s-patient-zero")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 2, 26, 0, 0, 0),
        title       = "China now has less new cases than the rest of the world",
        summary     = "For the first time, the reported number of new cases in China is exceeded by the ones outside of it.",
        description =
            "The WHO reports that for the first time since the discovery of the virus, "
          + "the number of new coronavirus cases inside of China is exceeded by the ones outside of it."
          + "The countries with the most new cases are Italy, Iran and South Korea, all of which saw a suddenly"
          + "increasing number of infected people.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("World Health Organization", "https://www.who.int/dg/speeches/detail/who-director-general-s-opening-remarks-at-the-mission-briefing-on-covid-19---26-february-2020")
        )
    )
    events += Event(
        time        = DateTime.of(2020, 3, 13, 0, 0, 0),
        title       = "Did it start even earlier?",
        description = "An unverified report suggests that the first case of the virus may have been found as early as the 17th of November 2019.",
        category    = Category.NEWS,
        sources     = listOf(
            Event.Source("South China Morning Post", "https://www.scmp.com/news/china/society/article/3074991/coronavirus-chinas-first-confirmed-covid-19-case-traced-back")
        )
    )
    events += Event(
        time     = DateTime.of(2020, 3, 14, 0, 0, 0),
        title    = "accuracy of reported cases is doubted",
        summary  = "The total number of cases is suspected to be much higher than reported, since a lot of infected do show very mild to no symptoms.",
        category = Category.NEWS,
        sources  = listOf(
            Event.Source("H. Lau, V. Khosrawipour; P. Kocbach, A. Mikolajczyk, H. Ichii, J. Schubert, J. Bania, T. Khosrawipour", "https://www.sciencedirect.com/science/article/pii/S1684118220300736")
        )
    )
    events += Event(
        time     = DateTime.of(2020, 3, 26, 0, 0, 0),
        title    = "the US is now the country with the most confirmed cases",
        summary  = "The United States has overtaken both China and Italy as the country with highest number of confirmed cases.",
        category = Category.NEGATIVE,
        sources  = listOf(
            Event.Source("The New York Times", "https://www.nytimes.com/2020/03/26/health/usa-coronavirus-cases.html")
        )
    )
    EventRepo.createAll(events)
}