package com.gitlab.covid19history.backend.graphql.resolvers

data class ValidationError(
    val field:   String,
    val message: String
)
