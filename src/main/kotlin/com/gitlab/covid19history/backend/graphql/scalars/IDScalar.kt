package com.gitlab.covid19history.backend.graphql.scalars

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import graphql.language.StringValue
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import org.bson.types.ObjectId
import org.litote.kmongo.Id
import org.litote.kmongo.id.toId
import graphql.schema.Coercing as GraphQLCoercing


object IDScalar {
    val type = GraphQLScalarType.newScalar(graphql.Scalars.GraphQLID)
        .coercing(Coercing)
        .build()

    val jacksonModule: Module = SimpleModule("IDModule")
        .addSerializer(Id::class.java,   Serializer)
        .addDeserializer(Id::class.java, Deserializer)

    private object Coercing : GraphQLCoercing<Id<*>, String> {
        override fun parseValue(input: Any?): Id<*> {
            if (input !is String) {
                throw CoercingParseValueException("unable to parse variable value $input as ID")
            }
            return ObjectId(input).toId<Any>()
        }

        override fun parseLiteral(input: Any?): Id<*> {
            if (input !is StringValue) {
                throw CoercingParseLiteralException("unable to parse literal $input as ID")
            }
            return ObjectId(input.value).toId<Any>()
        }

        override fun serialize(dataFetcherResult: Any?): String {
            if (dataFetcherResult is Id<*> || dataFetcherResult is ObjectId) {
                return dataFetcherResult.toString()
            }
            if (dataFetcherResult is String) {
                return dataFetcherResult
            }
            throw CoercingSerializeException("unable to serialize value $dataFetcherResult as DateTime")
        }
    }

    private object Serializer : StdSerializer<Id<*>>(Id::class.java) {
        override fun serialize(value: Id<*>, gen: JsonGenerator, provider: SerializerProvider) {
            gen.writeString(value.toString())
        }
    }

    private object Deserializer : StdDeserializer<Id<*>>(Id::class.java) {
        override fun deserialize(parser: JsonParser, ctx: DeserializationContext): Id<*> {
            val value = parser.valueAsString
            return ObjectId(value).toId<Any>()
        }
    }
}