package com.gitlab.covid19history.backend.graphql.inputs

object ReactionsData {
    data class Update(
        val claps:      Int?,
        val hearts:     Int?,
        val thumbUps:   Int?,
        val thumbDowns: Int?,
        val smiles:     Int?,
        val frowns:     Int?
    )
}