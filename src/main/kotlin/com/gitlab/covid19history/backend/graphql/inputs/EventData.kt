package com.gitlab.covid19history.backend.graphql.inputs

import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.utils.DateTime

object EventData {
    data class Create(
        val title:       String,
        val summary:     String?,
        val description: String?,
        val time:        DateTime,
        val sources:     List<Event.Source>,
        val category:    Event.Category
    )

    data class Filter(
        val before: DateTime?,
        val after:  DateTime?
    )
}