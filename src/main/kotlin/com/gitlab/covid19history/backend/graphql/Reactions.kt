package com.gitlab.covid19history.backend.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.data.repos.EventRepo
import com.gitlab.covid19history.backend.graphql.inputs.ReactionsData
import org.litote.kmongo.Id
import org.springframework.stereotype.Component

@Component
object ReactionMutations : GraphQLMutationResolver {
    suspend fun updateEventReactions(id: Id<Event>, data: ReactionsData.Update): Event.Reactions? {
        val original = EventRepo.find(id)
            ?: return null
        
        fun add(current: Int, add: Int?): Int {
            if (add == null) {
                return current
            }
            if (add < 0) {
                error("can't update a reaction with a negative amount")
            }
            return current + add
        }

        val event = original.copy(
            reactions = original.reactions.copy(
                claps      = add(original.reactions.claps,      data.claps),
                hearts     = add(original.reactions.hearts,     data.hearts),
                thumbUps   = add(original.reactions.thumbUps,   data.thumbUps),
                thumbDowns = add(original.reactions.thumbDowns, data.thumbDowns),
                smiles     = add(original.reactions.smiles,     data.smiles),
                frowns     = add(original.reactions.frowns,     data.frowns)
            )
        )
        EventRepo.update(event)
        return event.reactions
    }
}