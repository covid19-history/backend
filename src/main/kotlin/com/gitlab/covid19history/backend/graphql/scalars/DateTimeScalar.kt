package com.gitlab.covid19history.backend.graphql.scalars

import graphql.language.StringValue
import graphql.schema.CoercingParseLiteralException
import graphql.schema.CoercingParseValueException
import graphql.schema.CoercingSerializeException
import graphql.schema.GraphQLScalarType
import java.time.DateTimeException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import graphql.schema.Coercing as GraphQLCoercing

object DateTimeScalar {
    val type: GraphQLScalarType = GraphQLScalarType.newScalar()
        .name("DateTime")
        .coercing(Coercing)
        .build()

    object Coercing : GraphQLCoercing<LocalDateTime, String> {
        override fun parseValue(input: Any?): LocalDateTime {
            if (input !is String) {
                throw CoercingParseValueException("unable to parse variable value $input as DateTime")
            }
            try {
                return LocalDateTime.parse(input, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            } catch (e: DateTimeParseException) {
                throw CoercingParseValueException("$input is not a valid DateTime", e)
            }
        }

        override fun parseLiteral(input: Any?): LocalDateTime {
            if (input !is StringValue) {
                throw CoercingParseLiteralException("unable to parse literal $input as DateTime")
            }
            try {
                return LocalDateTime.parse(input.value, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            } catch (e: DateTimeParseException) {
                throw CoercingParseLiteralException("${input.value} is not a valid DateTime", e)
            }
        }

        override fun serialize(dataFetcherResult: Any?): String {
            if (dataFetcherResult !is LocalDateTime) {
                throw CoercingSerializeException("unable to serialize value $dataFetcherResult as DateTime")
            }
            try {
                return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(dataFetcherResult)
            } catch (e: DateTimeException) {
                throw CoercingSerializeException("failed to format $dataFetcherResult as DateTime")
            }
        }
    }
}