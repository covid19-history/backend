package com.gitlab.covid19history.backend.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.gitlab.covid19history.backend.data.models.Event
import com.gitlab.covid19history.backend.data.repos.EventRepo
import com.gitlab.covid19history.backend.data.validations.validateEvent
import com.gitlab.covid19history.backend.graphql.inputs.EventData
import com.gitlab.covid19history.backend.graphql.resolvers.Payload
import org.litote.kmongo.Id
import org.springframework.stereotype.Component

@Component
object EventQueries : GraphQLQueryResolver {
    suspend fun events(filter: EventData.Filter?): List<Event> {
        return EventRepo.list(after = filter?.after, before = filter?.before)
    }
}

@Component
object EventMutations : GraphQLMutationResolver {
    suspend fun createEvent(data: EventData.Create): Payload<Event> {
        val event = Event(
            title       = data.title,
            summary     = data.summary,
            description = data.description,
            time        = data.time,
            sources     = data.sources,
            category    = data.category
        )
        validateEvent(event).ifFailure {
            return Payload.Failure(it)
        }
        EventRepo.create(event)
        return Payload.Success(event)
    }

    suspend fun deleteEvent(id: Id<Event>): Boolean {
        return EventRepo.delete(id)
    }
}