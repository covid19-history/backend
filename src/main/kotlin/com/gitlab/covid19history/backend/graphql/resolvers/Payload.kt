package com.gitlab.covid19history.backend.graphql.resolvers

import com.gitlab.covid19history.backend.data.validations.lib.VName
import com.gitlab.covid19history.backend.data.validations.lib.VResult
import kotlin.reflect.KProperty1
import kotlin.reflect.jvm.javaField

interface Payload<T : Any> {
    val result: T?
    val errors: List<ValidationError>

    class Success<T : Any>(
        override val result: T
    ) : Payload<T> {
        override val errors = emptyList<ValidationError>()
    }

    class Failure<T : Any>(
        override val errors: List<ValidationError>
    ) : Payload<T> {
        override val result: T? = null

        companion object {
            operator fun <T : Any> invoke(field: String, message: String) =
                Failure<T>(
                    listOf(
                        ValidationError(
                            field,
                            message
                        )
                    )
                )

            operator fun <T : Any> invoke(failure: VResult.Failure<T>): Failure<T> {
                return Failure(
                    errorsOf(
                        failure
                    )
                )
            }

            private fun <T> errorsOf(failure: VResult.Failure<T>, prefix: String = ""): List<ValidationError> {
                val errors = mutableListOf<ValidationError>()
                for (rule in failure.failedRules) {
                    errors += ValidationError(
                        prefix,
                        rule.message
                    )
                }
                val prefixWithDot = if (prefix.isEmpty()) {
                    prefix
                } else {
                    "$prefix."
                }

                for ((field, fieldFailure) in failure.failedFields) {
                    val propErrors =
                        errorsOf(
                            fieldFailure,
                            prefix = prefixWithDot + nameOf(field)
                        )
                    errors += propErrors
                }
                return errors
            }

            private fun nameOf(prop: KProperty1<*, *>): String {
                val vName = prop.javaField?.annotations?.find { it is VName }
                    ?: return prop.name
                return (vName as VName).value
            }
        }
    }
}