package com.gitlab.covid19history.backend.graphql

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

@Component
object Query : GraphQLQueryResolver {
    val version = "0.0.1"
}