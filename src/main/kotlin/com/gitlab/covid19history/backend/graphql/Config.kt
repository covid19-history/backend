package com.gitlab.covid19history.backend.graphql

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.covid19history.backend.graphql.scalars.DateTimeScalar
import com.gitlab.covid19history.backend.graphql.scalars.IDScalar
import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class Config(
    objectMapper: ObjectMapper
) {
    init {
        objectMapper.registerModule(IDScalar.jacksonModule)
    }

    @Bean
    open fun scalars(): Array<GraphQLScalarType> {
        return arrayOf(
            IDScalar.type,
            DateTimeScalar.type
        )
    }
}