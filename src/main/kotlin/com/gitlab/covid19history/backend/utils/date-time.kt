package com.gitlab.covid19history.backend.utils

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset

typealias DateTime = LocalDateTime
typealias Date     = LocalDate

fun nowDateTime(): DateTime {
    return LocalDateTime.now(ZoneOffset.UTC)
}