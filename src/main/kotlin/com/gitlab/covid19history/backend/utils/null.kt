package com.gitlab.covid19history.backend.utils

inline fun <T, TResult> T?.ifPresent(action: (T) -> TResult): TResult? {
    if (this == null) {
        return null
    }
    return action(this)
}