package com.gitlab.covid19history.backend.utils.props

import java.util.*
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.KTypeParameter

class IndexProperty<T, out R>(
    val index:       Int,
    private val get: T.(Int) -> R?
) : KProperty1<T, R?> {
    override fun invoke(value: T): R? {
        return value.get(index)
    }

    override val annotations      = emptyList<Annotation>()
    override val getter     get() = error("not implemented")
    override val isAbstract       = false;
    override val isConst          = false;
    override val isFinal          = false;
    override val isLateinit       = false;
    override val isOpen           = false;
    override val isSuspend        = false;
    override val name             = index.toString()
    override val parameters       = emptyList<KParameter>()
    override val typeParameters   = emptyList<KTypeParameter>()
    override val visibility       = null
    override val returnType get() = error("not implemented")
    override fun call(vararg args: Any?): R =
        error("not implemented")

    override fun callBy(args: Map<KParameter, Any?>): R =
        error("not implemented")

    override fun get(receiver: T): R? =
        invoke(receiver)

    override fun getDelegate(receiver: T): Any? =
        null

    override fun hashCode(): Int {
        return Objects.hash(index, get)
    }

    override fun equals(other: Any?): Boolean {
        return false
    }

    override fun toString(): String {
        return "${IndexProperty::class.simpleName}($index)"
    }
}