FROM gradle:6.3.0-jdk14 as builder

WORKDIR /covid19-history/backend

ADD . .
RUN gradle clean bootJar

#=== FINAL ===#

FROM openjdk:14

ARG VERSION

WORKDIR /covid19-history/backend

COPY --from=builder /covid19-history/backend/build/libs/covid19-history.backend-${VERSION}.jar covid19-history.backend.jar

CMD [ "java", "-jar", "covid19-history.backend.jar" ]