plugins {
    id("java")
    id("idea")

    kotlin("jvm") version "1.3.70"

    id("org.springframework.boot") version "2.2.4.RELEASE"
}

apply(plugin = "io.spring.dependency-management")

group = "pfadiolten.ch"
version = "0.0.2"

repositories {
    mavenCentral()
    jcenter()
    mavenLocal()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("com.graphql-java-kickstart:graphql-spring-boot-starter:6.0.1")
    implementation("com.graphql-java-kickstart:graphql-kickstart-spring-boot-starter-tools:6.0.1")

    implementation("com.graphql-java-kickstart:playground-spring-boot-starter:6.0.1")
    implementation("com.graphql-java-kickstart:voyager-spring-boot-starter:6.0.1")

    implementation("org.litote.kmongo:kmongo-coroutine:3.12.0")

    implementation("com.github.kittinunf.fuel:fuel-coroutines:2.2.1")
    implementation("com.github.kittinunf.fuel:fuel-coroutines:2.2.1")
    implementation("com.google.code.gson:gson:2.8.6")

    implementation("com.googlecode.owasp-java-html-sanitizer:owasp-java-html-sanitizer:20191001.1")
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}